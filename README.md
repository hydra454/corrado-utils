# Welcome to Corrado Utils!
This Package contains all the additions I found useful for myself.
Any suggestion on new additions is welcome!

# Features
* **Sort**: Located under Edit→Lines, allows the alphabetical sorting of the lines in the current file.
* **Remove Duplicates**: Located under Edit→Lines, allows the removal of the duplicate lines in the current file, preserving the order in which the first occurrence is entered

![A screenshot of your package](https://f.cloud.github.com/assets/69169/2290250/c35d867a-a017-11e3-86be-cd7c5bf3ff9b.gif)
