'use babel';

export default class CorradoUtilsView {
//Keeping this class as it will be needed not far in the future.
  constructor(serializedState) {
    // Create root element
    this.element = document.createElement('div');
    this.element.classList.add('corrado-utils');

    // Create message element
    const message = document.createElement('div');
    message.textContent = 'The CorradoUtils package is Alive! It\'s ALIVE!';
    message.classList.add('message');
    this.element.appendChild(message);
  }

  // Returns an object that can be retrieved when package is activated
  serialize() {}

  // Tear down any state and detach
  destroy() {
    this.element.remove();
  }

  getElement() {
    return this.element;
  }

}
