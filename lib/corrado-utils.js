'use babel';

import CorradoUtilsView from './corrado-utils-view';
import { CompositeDisposable } from 'atom';

export default {

  corradoUtilsView: null,
  modalPanel: null,
  subscriptions: null,

  activate(state) {
    this.corradoUtilsView = new CorradoUtilsView(state.corradoUtilsViewState);
    this.modalPanel = atom.workspace.addModalPanel({
      item: this.corradoUtilsView.getElement(),
      visible: false
    });

    // Events subscribed to in atom's system can be easily cleaned up with a CompositeDisposable
    this.subscriptions = new CompositeDisposable();

    // Register commands
    this.subscriptions.add(atom.commands.add('atom-workspace', {
      'corrado-utils:sort': () => this.sort()
    }));
    this.subscriptions.add(atom.commands.add('atom-workspace', {
      'corrado-utils:removeDuplicates': () => this.removeDuplicates()
    }));
  },

  deactivate() {
    this.modalPanel.destroy();
    this.subscriptions.dispose();
    this.corradoUtilsView.destroy();
  },

  serialize() {
    return {
      corradoUtilsViewState: this.corradoUtilsView.serialize()
    };
  },

  sort() {
   console.log('CorradoUtils->Sort was called!');
   const editor = atom.workspace.getActiveTextEditor();
   if(editor){
     const buffer = editor.getBuffer();
     buffer.setText(buffer.getText().split("\n").sort().join("\n"));
   }
 },
 removeDuplicates(){
   const editor = atom.workspace.getActiveTextEditor();
   if(editor){
     const buffer = editor.getBuffer();
     let dictionary = [];
     buffer.getText().split('\n').forEach((item, i) => {
       if(!dictionary.includes(item)){
         dictionary.push(item)
       }
     });
     buffer.setText(dictionary.join('\n'));
   }
 }
};
